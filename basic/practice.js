// Palindrome Number

/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function(x) {
    // return String(x) === String(x).split('').reverse().join('');
    const reverse = String(x).split('').reduce((a,b)=>{
        return b+=a
    },'');
    if(reverse==String(x)) return true;
    return false
};

// Time complexity = O(log10(x))
// Two Sum

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    // for(let i=0;i<nums.length-1;i++){
    //     for(let j=i+1; j<nums.length; j++){
    //         if(nums[i]+nums[j] == target){
    //             return [i,j]
    //         }
    //     }
    // };
    const ans = {};
    for(let i=0;i<nums.length;i++){
        let x = target - nums[i];

        if(ans.hasOwnProperty(x)){
            return [ans[x],i];
        };
        ans[nums[x]] = i;
    }
    return []
};

// Time complexity = o(n^2); 
// Longest Common Prefix

/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
    
    let i = 1;
    let j = 0;
    let ans=""
    if(strs.length>1){
    while(true){
        if(strs[i][j] && strs[i][j] == strs[i-1][j]){
            if(i<strs.length-1){
                i+=1
            }else{
                ans+=strs[i][j]
                i=1
                j+=1
            }
        }else{
            break
        }
    }
    }else if(strs.length==1){
        return strs.join()
    }
    return ans
};

// Time Complexity = o(s) 
// Remove Element

/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function(nums, val) {
    
    while(true){
        if(nums.includes(val)){
           nums.splice(nums.indexOf(val),1);
        }else{
            break
        }
        
    };
    console.log(nums)
};
// Time complexity = o(n^2)
// Find the Index of the First Occurrence in a String

/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function(haystack, needle) {
    for(let i=0;i<haystack.length-needle.length+1;i++){
        if(haystack.slice(i,needle.length+i) == needle){
            return i
        }
    }
    return -1

};

// Time complexity = o((m-n-1)*n)